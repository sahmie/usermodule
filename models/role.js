
module.exports = (sequelize, Sequelize) => {
  var Role = sequelize.define('Role', {
    id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    role_name: {
       type: Sequelize.STRING
    }
  });

    Role.associate = function(models) {
    models.Role.hasMany(models.User);
  };

  return Role;
};
