
module.exports = (sequelize, Sequelize) => {
  var Profile = sequelize.define('Profile', {
     id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    profile_name: {
       type: Sequelize.STRING
    }
  });

    Profile.associate = function(models) {
    models.Profile.hasMany(models.User);
  };

  return Profile;
};
