
module.exports = function(sequelize, Sequelize){
  var CurrentBusiness = sequelize.define('CurrentBusiness', {
       id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
     name: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true
        },
        phone: {
            type: Sequelize.INTEGER
        },
        address: {
            type: Sequelize.STRING,
        },
        website : {
        type: Sequelize.STRING,
        allowNull: true
        },
        country: {
        type: Sequelize.STRING,
        allowNull: true
        },
  });

    CurrentBusiness.associate = function(models) {
    models.CurrentBusiness.hasMany(models.User);
  };

  return CurrentBusiness;
};
