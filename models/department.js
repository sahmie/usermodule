
module.exports = (sequelize, Sequelize) => {
  var Department = sequelize.define('Department', {
     id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
    },
    department_name: {
       type: Sequelize.STRING
    }
  });

    Department.associate = function(models) {
    models.Department.hasMany(models.User);
  };

  return Department;
};
